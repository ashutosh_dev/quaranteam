#!/usr/bin/env python
# coding: utf-8

# In[8]:


import pickle
import pandas as pd
import os
from sklearn.ensemble import RandomForestClassifier as xgb
#from xgboost import XGBClassifier as xgb
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
import scipy.signal as signal
import numpy as np
from datetime import datetime
import json
import requests


# In[2]:


def spectral_features(sample, num_freq = 5):
    sample_pd_x = pd.DataFrame(signal.welch(sample.x)[1])
    sample_pd_x.columns = ['x']
    x_f = np.append(sample_pd_x.nlargest(5, ['x']).to_numpy(), sample_pd_x.nlargest(5, ['x']).index.values)
    sample_pd_y = pd.DataFrame(signal.welch(sample.y)[1])
    sample_pd_y.columns = ['y']
    y_f = np.append(sample_pd_y.nlargest(5, ['y']).to_numpy(), sample_pd_y.nlargest(5, ['y']).index.values)
    sample_pd_z = pd.DataFrame(signal.welch(sample.z)[1])
    sample_pd_z.columns = ['z']
    z_f = np.append(sample_pd_z.nlargest(5, ['z']).to_numpy(), sample_pd_z.nlargest(5, ['z']).index.values)
    temp_f = np.append(x_f, y_f)
    acc_f = np.append(temp_f, z_f)
    return acc_f

#y = []


# In[4]:


df = pd.DataFrame()
for i in os.listdir('hack_data'):
    dat = pd.read_csv(str('hack_data/'+i), header=None)
    #print(i)
    if(dat.shape[1] == 4):
        dat.columns = ['id', 'x', 'y', 'z']
    else:
        dat.columns = ['x', 'y', 'z']
    dat = dat[['x', 'y', 'z']]
    df = df.append(pd.DataFrame(spectral_features(dat)).T)
    #y.append(2)
df.to_csv('hack_transformed/transformed.csv', index=False)
df.index = os.listdir('hack_data')


# In[5]:


pickle_in = open("dict.pickle","rb")
example_dict = pickle.load(pickle_in)
pred =example_dict.predict(df) 
pickle_in.close()


# In[10]:


url = "https://rio-staging.miqdigital.com/quaranteam-ashutosh-0.0.1/update-result"
headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "a0905aff-bfef-4173-3f5f-a2f94af78fdf"
    }


# In[11]:


name = ['Officer1 (Punching_demo)', 'Officer2 (Walking_demo)']
action = []
colour = []
#pred[0]=0
for i in range(0,len(pred)):
    if pred[i] == 0:
        action.append('Walking')
        colour.append('Green')
    elif pred[i] == 1:
        action.append('Running')
        colour.append('Orange')
    else:
        action.append('Punching')
        colour.append('Red')

    payload1 = {
    'name':name[i],
    'timestamp':str(datetime.now()),
    'action':action[i],
    'colour':colour[i]
    }

    payload_json = json.dumps(payload1) 
    response = requests.request("POST", url, data=payload_json, headers=headers)
    


# In[ ]:





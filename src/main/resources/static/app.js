var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#userinfo").html("");
}

function connect() {
	debugger;
	var xhr = new XMLHttpRequest();
	var url = "https://rio-staging.miqdigital.com/quaranteam-ashutosh-0.0.1/trigger-predict"
//	var url = "http://localhost:9091/trigger-predict"
	xhr.open("GET", url);
	xhr.setRequestHeader("Content-type", "application/json");	
	var data = JSON.stringify({
		  "name": "Ashu",
		  "timestamp": "8 30",
		  "action": "Walking",
		  "colour": "Red"
		});
	
	xhr.send(data);
	
//    var socket = new SockJS('/quaranteam-ashutosh-0.0.1/websocket-example');
//    var socket = new SockJS('/websocket-example');
//    stompClient = Stomp.over(socket);
//    stompClient.connect({}, function (frame) {
//        setConnected(true);
//        console.log('Connected: ' + frame);
//        stompClient.subscribe('/topic/user', function (greeting) {
//            showGreeting(JSON.parse(greeting.body));
//        });
//    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    
    
    
    console.log("Disconnected");
}

function sendName() {
    stompClient.send("/app/user", {}, JSON.stringify({'name': $("#name").val()}));
}

function showGreeting(message) {
	{"Walking", "Driving", "Running", "Tussle", "Gunshot"}
	if(message.action == "Walking" || message.action == "Sitting")
		$("#userinfo").prepend("<tr class='table-success' style='background: seashell;'><td>" + message.name + "</td><td>" + message.timestamp + "</td><td>" + message.action + "</td><td style='background: green;'>" + "" + "</td></tr>");
	if(message.action == "Running")
		$("#userinfo").prepend("<tr class='table-warning' style='background: seashell;'><td>" + message.name + "</td><td>" + message.timestamp + "</td><td>" + message.action + "</td><td style='background: yellow;'>" + "" + "</td></tr>");
	if(message.action == "Tackle" || message.action == "Punch" || message.action == "Punching")
		$("#userinfo").prepend("<tr class='table-danger' style='background: seashell;'><td>" + message.name + "</td><td>" + message.timestamp + "</td><td>" + message.action + "</td><td style='background: red;'>" + "" + "</td></tr>");
	
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});

$(document).ready(function() {
	var socket = new SockJS('/quaranteam-ashutosh-0.0.1/websocket-example');
//	var socket = new SockJS('/websocket-example');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
//        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/user', function (greeting) {
            showGreeting(JSON.parse(greeting.body));
        });
    });
    
  });
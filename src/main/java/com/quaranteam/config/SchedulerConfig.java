package com.quaranteam.config;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.tomcat.jni.Time;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quaranteam.model.PredictResponse;
import com.quaranteam.model.UserResponse;

@EnableScheduling
@Configuration
public class SchedulerConfig {

    @Autowired
    SimpMessagingTemplate template;

//    @Scheduled(fixedDelay = 500)
//    public void sendAdhocMessages() throws JsonProcessingException, InterruptedException {
//    	String[] users = new String[] {"Diptarka","Biddappa","Aishwarya", "Stephanie", "Lucy", "Meenakshi", "Prateek","Ashutosh","Anshul","Parth","Biddappa"};
//    	String[] actions = new String[] {"Walking", "Sitting", "Running", "Tussle", "Walking", "Punch", "Running", "Running","Tackle", "Driving"};
//    	int rand_num = new Random().nextInt(3);
//
////    	String user = users[(int)(System.currentTimeMillis() % users.length)];
//    	String user = users[new Random().nextInt(users.length)];
//    	String action = actions[new Random().nextInt(actions.length)];
//    	String timestamp = getTimeStamp();
//    	template.convertAndSend("/topic/user", new PredictResponse(user, timestamp, action,"Red"));
//    	Thread.sleep(rand_num*900);
////    	template.convertAndSend("/topic/user", new PredictResponse("Ashu", "4 30", "Walking","Red"));
////        template.convertAndSend("/topic/user", new UserResponse("Fixed Delay Scheduler"));
//    }
    
    public String getTimeStamp() {
    	LocalDateTime now = LocalDateTime.now();
    	int year = now.getYear();
    	int month = now.getMonthValue();
    	int day = now.getDayOfMonth();
    	int hour = now.getHour();
    	int minute = now.getMinute();
    	int second = now.getSecond();
    	int millis = now.get(ChronoField.MILLI_OF_SECOND);
//    	String timeString = String.format("%02d:%02d:%02d",hour,minute,second); 
    	String timeString = String.format("%d-%02d-%02d %02d:%02d:%02d.%03d", year, month, day, hour, minute, second, millis);
    	return timeString;
    }
}

package com.quaranteam.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quaranteam.model.PredictResponse;
import com.quaranteam.model.User;
import com.quaranteam.model.UserResponse;
import com.quaranteam.processor.PredictionProcessor;

@RestController
public class PedictionController {
	
	@Autowired
    SimpMessagingTemplate template;
	
	@Autowired
	PredictionProcessor predictionProcessor;


    
    public UserResponse getUser(User user) throws JsonProcessingException {
    	ObjectMapper Obj = new ObjectMapper(); 
    	PredictResponse prd = new PredictResponse("Ashu", "4 30", "Walking","Red");
    	String jsonStr = Obj.writeValueAsString(prd);
    	template.convertAndSend("/topic/user", jsonStr);
        return new UserResponse("Hi " + user.getName());
    }
    
    @RequestMapping(value = "/update-result", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<PredictResponse> saveRequest(@Valid @RequestBody PredictResponse predictResponse) {
    	// request = {"name":"Ashu","timestamp":"4 30","action":"Walking","colour":"Red"}
    	template.convertAndSend("/topic/user", predictResponse);
		return new ResponseEntity<PredictResponse>(predictResponse, HttpStatus.CREATED);
	}
    
    @RequestMapping(value = "/trigger-predict", method = RequestMethod.GET)
	public ResponseEntity<String> trigger() {
    	
    	String output = predictionProcessor.recommendationProcess();
    	
	  //TODO:fetch user info from token
		return new ResponseEntity<String>(output, HttpStatus.OK);
	}
    
}

package com.quaranteam.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quaranteam.model.PredictResponse;
import com.quaranteam.model.User;
import com.quaranteam.model.UserResponse;

@Controller
public class UserController {
	
	@Autowired
    SimpMessagingTemplate template;


    @MessageMapping("/user")
    @SendTo("/topic/user")
    public UserResponse getUser(User user) throws JsonProcessingException {
    	ObjectMapper Obj = new ObjectMapper(); 
    	PredictResponse prd = new PredictResponse("Ashu", "4 30", "Walking","Red");
    	String jsonStr = Obj.writeValueAsString(prd);
    	template.convertAndSend("/topic/user", jsonStr);
        return new UserResponse("Hi " + user.getName());
    }
}

/**
 * @author ashutosh@miqdigital.com
 */

package com.quaranteam.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class PredictionProcessor {

	@Value("${rio.dsRioPath.dir}")
	private String dsRioPathName;
	@Value("${rio.recoScript.name}")
	private String recoScriptName;

	private static final Logger LOG = LoggerFactory.getLogger(PredictionProcessor.class);

	@Autowired
	private Environment env;

	public String recommendationProcess() {

		try {
			Process predictionStat;
			List<String> predictionArgumentsList = fetchArgumentList();

			try {
				predictionStat = new ProcessBuilder()
						.command(predictionArgumentsList.toArray(new String[predictionArgumentsList.size()]))
						.inheritIO().start();

				int exitCode = predictionStat.waitFor();
				if (exitCode != 0) {
					Thread.sleep(5000);
				}

			} catch (Exception ex) {

			}
			return predictionArgumentsList.toString();
		}

		catch (Exception e) {
			LOG.error("Inside generic Exception block at Recommendation Processor");
		}
		return dsRioPathName;
	}

	private List<String> fetchArgumentList() throws NumberFormatException, JSONException {

		List<String> argumentsList = new ArrayList<String>();
		argumentsList.add("bash");
		argumentsList.add("-c");
		String commandToRun = "cd " + dsRioPathName + ";cp " + recoScriptName + " tmp/" + recoScriptName + ";./tmp/"
				+ recoScriptName;
		LOG.info("The command to run at shell is -------------------" + commandToRun);
		argumentsList.add(commandToRun);
		System.out.println(commandToRun);
		return argumentsList;

//		cd location
//		cp script.py tmp/script.sh
//		sh tmp/script.sh

	}

}

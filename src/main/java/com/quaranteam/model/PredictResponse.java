package com.quaranteam.model;

public class PredictResponse {
    String name;
    String timestamp;
    String action;
    String colour;

    public PredictResponse() {
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public PredictResponse(String name, String timestamp, String action, String colour) {
		super();
		this.name = name;
		this.timestamp = timestamp;
		this.action = action;
		this.colour = colour;
	}

    
}
